1. Дань уважения истории.
	(i)   Реализовать персептрон, обучить на MNIST.
		  https://www.tensorflow.org/get_started/mnist/beginners

	(ii)  Построить и реализовать сеть, вычисляющую XOR от двух входов.



2. NN как модель вычислений.
	(i)   Реализовать линейную регрессию с помощью нейронной сети.
		  https://www.tensorflow.org/get_started/get_started

	(ii)  Реализовать логистическую регрессию с помощью нейронной сети.
		  http://deeplearning.net/tutorial/logreg.html


3. Представление.
	(i)   Нейронные сети как инструмент снижения размерности. Реализовать классификатор рукописных цифр (MNIST) с одним слоем и двумя узлами.

	(ii)  Автоэнкодеры (autoencoders).
		* Реализовать AE с одним слоем без нелинейностей. Сравнить с PCA и (i).
		* Реализовать AE с одним слоем без нелинейностей. Сравнить с PCA и (i), (ii).
		* Denoising AE.
			http://deeplearning.net/tutorial/dA.html

4. Архитектуры.
	(iii) Свёрточные сети.
		* Детектирование и классификация объектов, семантическая сегментация.
		  http://www.robots.ox.ac.uk/~szheng/CRFasRNN.html
		  http://host.robots.ox.ac.uk/pascal/VOC/

	(iv)  Рекуррентные сети.
		  http://deeplearning.net/tutorial/rnnslu.html

	(v)   LSTM.
		  http://blog.echen.me/2017/05/30/exploring-lstms/?utm_source=Mailing+list&utm_campaign=479f35fbcc-Kaggle_Newsletter_06-29-2017&utm_medium=email&utm_term=0_f42f9df1e1-479f35fbcc-399206381
		  http://deeplearning.net/tutorial/lstm.html
		  https://www.youtube.com/playlist?list=PL2-dafEMk2A5-sn0Sgkw-4q-Lw0jiuQtu


Книги:
	* Ethem Alpaydin. Introduction to Machine Learning.
	* Ian Goodfellow, Yoshua Bengio, Aaron Courville. Deep Learning. http://www.deeplearningbook.org/
	* Christopher M. Bishop. Neural Networks for Pattern Recognition.
	* Michael Nielsen. Neural Networks and Deep Learning. http://neuralnetworksanddeeplearning.com/
	* Adam Gibson, Josh Patterson. Deep Learning: A Practitioner's Approach
