
# coding: utf-8

# In[37]:

import numpy as np
import matplotlib.pyplot as plt


# In[38]:

true_slope = 3
true_intercept = 0.3

data_x = np.linspace(-1,1,6)
data_y = true_slope*data_x + true_intercept

x_train = data_x[0:3]
y_train = data_y[0:3]

x_test = data_x[4:5]
y_test = data_y[4:5]



# In[39]:

import tensorflow as tf


# In[40]:

lrate = 0.5
steps_cnt = 100
logs_path = '/tmp/sciuru/linear_reg'
log_freq = 5
batch_sz = x_train.shape[0]


# In[41]:

tf.reset_default_graph()

with tf.name_scope("input"):
    
    x = tf.placeholder(tf.float32, [None])
    y_ = tf.placeholder(tf.float32, shape=[None])

with tf.name_scope('weights'):
    
    W = tf.Variable(0.01)
    b = tf.Variable(0.01)

with tf.name_scope('output'):    
    y = x*W + b

    
with tf.name_scope('loss'): 
    
    mse = tf.reduce_mean(tf.square(y-y_))

with tf.name_scope('train'): 
    
    train_step = tf.train.GradientDescentOptimizer(lrate).minimize(mse)
    
summ_mse = tf.summary.scalar('mean_squared_error', mse)


# In[31]:

log_cnt = 0


# In[32]:

sess = tf.Session()
sess.run(tf.global_variables_initializer())


# In[33]:


writer = tf.summary.FileWriter('%s/run%d' % (logs_path, log_cnt), graph=sess.graph)
log_cnt = log_cnt+1
    

for step in range(steps_cnt):
        
    inds = np.arange(batch_sz)
    np.random.shuffle(inds)
    
    
    sess.run(train_step, feed_dict = {x: x_train[inds], y_: y_train[inds]})
    
    if step%log_freq ==0:
            
        loss, summ = sess.run([mse, summ_mse], feed_dict={x: x_test, y_: y_test})
         
        writer.add_summary(summ, step/log_freq)
    
        print("Step %d Loss %f" % (step, loss))
        
writer.close()


# In[34]:

W_val,b_val = sess.run([W,b])


# In[35]:

print("(true)  slope %.5f, intercept %.5f" % (true_slope,true_intercept))
print("(found) slope %.5f, intercept %.5f" % (W_val,b_val))


# In[36]:

sess.close()


# In[ ]:



