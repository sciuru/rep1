
# coding: utf-8

# In[19]:

import tensorflow as tf


# In[20]:

tf.reset_default_graph()

x_ = tf.placeholder(tf.float32, shape=[None,2], name='x_')
y_ = tf.placeholder(tf.float32, shape=[None,1], name='y_')

W1 = tf.Variable(tf.random_uniform([2,2], -1, 1), name = "W1")
W2 = tf.Variable(tf.random_uniform([2,1], -1, 1), name = "W2")

b1 = tf.Variable(tf.zeros([2]), name = "b1")
b2 = tf.Variable(tf.zeros([1]), name = "b2")

with tf.name_scope("layer2"):
    y1 = tf.sigmoid(tf.matmul(x_, W1) + b1)

with tf.name_scope("layer3"):
    y = tf.sigmoid(tf.matmul(y1, W2) + b2)

with tf.name_scope("cost"):
    cost = -tf.reduce_mean((y_ * tf.log(y)) + ((1 - y_) * tf.log(1 - y)))

with tf.name_scope("train"):
    train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cost)

xs = [[0,0],[0,1],[1,0],[1,1]]
ys = [[0],[1],[1],[0]]


sess = tf.Session()

writer = tf.summary.FileWriter("/tmp/sciuru/xor", sess.graph)

sess.run(tf.global_variables_initializer())


# In[21]:

for i in range(100000):
    sess.run(train_step, feed_dict={x_: xs, y_: ys})
    if i % 1000 == 0:
        print('%d  cost %f' % (i, sess.run(cost, feed_dict={x_: xs, y_: ys})))


# In[24]:

print(xs)
print([ "%.4f" % x for x in sess.run(y, feed_dict={x_: xs, y_: ys}) ])


# In[10]:

writer.close()
sess.close()


# In[ ]:



