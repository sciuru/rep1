
# coding: utf-8

# In[3]:

import numpy as np
import matplotlib.pyplot as plt


# In[4]:

import struct

def read_idx(filename):
    with open(filename, 'rb') as f:
        zero, data_type, dims = struct.unpack('>HBB', f.read(4))
        shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
        return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)


# In[5]:

train_x     = read_idx("train-images-idx3-ubyte")
train_y_arr = read_idx("train-labels-idx1-ubyte")
test_x      = read_idx("t10k-images-idx3-ubyte")
test_y_arr  = read_idx("t10k-labels-idx1-ubyte")


# In[6]:

train_x = train_x.reshape([60000,28*28])
test_x = test_x.reshape([10000,28*28])


# In[7]:

def one_hot(arr):
    
    res = np.zeros([arr.shape[0],10])
    res[np.arange(arr.shape[0]),arr] = 1
    return res


# In[8]:

train_y = one_hot(train_y_arr)
test_y = one_hot(test_y_arr)


# In[10]:

# display random image (check if data reshaped etc properly) 

ix = np.random.randint(0,60000)
img = train_x[ix]
label = np.argmax(train_y[ix])

plt.title('Image: %d Label: %d' % (ix, label))
plt.imshow(train_x[ix].reshape([28, 28]), cmap = plt.get_cmap('gray_r'))
plt.show()


# In[11]:

import tensorflow as tf


# In[12]:

lrate = 0.5
steps_cnt = 5000
logs_path = '/tmp/sciuru/mnist'
log_freq = 20
batch_sz = 100


# In[13]:

tf.reset_default_graph()

with tf.name_scope("input"):
    
    x = tf.placeholder(tf.float32, [None, 784])
    y_ = tf.placeholder(tf.float32, shape=[None, 10])

with tf.name_scope('weights'):
    
    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))

with tf.name_scope('output'):    
    y = tf.matmul(x,W) + b
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    
with tf.name_scope('train'): 
    train_step = tf.train.GradientDescentOptimizer(lrate).minimize(cross_entropy)

with tf.name_scope('acc'): 
    
    correct_cnt = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_cnt, tf.float32))
    
tf.summary.scalar('cost', cross_entropy)
tf.summary.scalar('acc', accuracy)
summary_op = tf.summary.merge_all()


# In[15]:

#cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))


# In[14]:

log_cnt = 0


# In[15]:

sess = tf.Session()
sess.run(tf.global_variables_initializer())


# In[16]:


# each Run in separate folder
writer = tf.summary.FileWriter('%s/run%d' % (logs_path, log_cnt), graph=sess.graph)
log_cnt = log_cnt+1
    

for step in range(steps_cnt):
        
    inds = np.arange(60000)
    np.random.shuffle(inds)
    inds = inds[0:batch_sz]
    
    sess.run(train_step, feed_dict = {x:train_x[inds], y_: train_y[inds]})
    
    if step%log_freq ==0:
            
        loss, acc, summ = sess.run([cross_entropy, accuracy, summary_op], feed_dict={x:test_x, y_: test_y})
         
        writer.add_summary(summ, step/log_freq)
    
        print("Step %d  Accuracy %f  Loss %f" % (step, acc, loss))
        
writer.close()


# In[18]:

ce, acc = sess.run([cross_entropy, accuracy], feed_dict={x:test_x, y_: test_y})
print("cross-e = %f accuracy = %f" % (ce, acc))
    


# In[40]:

#  NO CODE HERE 
#
# from previous experiments, for comparison


# In[103]:

W_val = sess.run(W)
for i in range(10):
    plt.title(str(i))
    img = np.reshape(W_val[:,i], [28, 28])
    plt.imshow(img, cmap=plt.get_cmap('seismic'))
    plt.show()


# In[ ]:

sess.close()

